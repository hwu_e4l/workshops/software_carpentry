<!--- Local IspellDict: en -->
<!--- SPDX-FileCopyrightText: 2017-2020,2022 Jens Lechtenbörger -->
<!--- SPDX-License-Identifier: CC-BY-SA-4.0 -->

# Introduction

This project hosts source files for
[Open Educational Resources (OER)](https://en.wikipedia.org/wiki/Open_educational_resources)
for a course on Operating Systems as part of the module
[Computer Structures and Operating Systems](https://www.wi.uni-muenster.de/student-affairs/course-offerings/261893)
(mandatory module in the 4th term of the Bachelor program in
[Information Systems at the University of Münster, Germany](https://www.wi.uni-muenster.de/)),
starting with summer term 2017 and repeated in subsequent years.
The source files and software of this repository are used to generate
[HTML presentations with embedded audio explanations supporting my course on Operating Systems hosted as GitLab pages](https://oer.gitlab.io/OS).

Starting with my lectures on Operating Systems in 2017,
I changed two aspects in contrast to previous years, namely (1)
teaching strategy and (2) distribution of educational resources.
These changes were possible thanks to a
[fellowship for innovation in digital university teaching](https://www.stifterverband.org/lehrfellows/2016/lechtenboerger)
funded by the
Ministry of Innovation, Science and Research of the State of North
Rhine-Westphalia, Germany, and [Stifterverband](https://www.stifterverband.org).


# Just-in-Time Teaching

First, I applied
[Just-in-Time Teaching](https://jittdl.physics.iupui.edu/jitt/) (JiTT;
see
[here for the Wikipedia entry](https://en.wikipedia.org/wiki/Just_in_Time_Teaching))
as teaching and learning strategy, where students prepare class
meetings at home.  In a nutshell, students work through educational
resources (texts, presentations, videos, etc.) on their own and submit
individual solutions to pre-class assignments (so-called warm-up
exercises).  Students’ solutions are corrected prior
to class meetings to identify misunderstandings and incorrect prior
beliefs.  Based on those finding, class meetings are adjusted
just-in-time to create a feedback loop with increased students’ learning.

In my view, the essential shortcoming of traditional lectures is as
follows: I’m mostly talking based on slides (with some interspersed
activities and questions), while students are listening, taking
notes, talking, dreaming, or playing with their phones; usually, there are
only few questions and little interaction.  When the exam phase draws
close, students start to work through the slides again and discover
difficulties.  However, then I’m not present for questions any more,
and students are on their own (although there is an online course
forum, which is used for questions by few).

I’d like to share one result of a previous year’s multiple-choice poll
that was executed anonymously via a classroom response system and that
formed the turning point at which I decided to change my teaching.
The poll addressed students’ understanding of mutual exclusion (MX) in
Java via the keyword `synchronized` (which is a challenging topic):

|No. of participants | % of participants | Response option         |
|----|--------|----------------------------------------------------|
| 14 | 37.84% | I can explain Java MX via synchronized.            |
|  5 | 13.51% | I’m confused, and I’m willing to ask questions.    |
| 18 | 48.65% | I’m confused, but I’m not willing to ask questions.|

Although I spent three lectures explaining MX in general and MX in
Java in particular (investing about twice as much time as in previous
years), only a minority of students (about 38%) were confident that
they had understood the topic.  Worse yet, among students that had
trouble grasping this topic, the vast majority (18 out of 23) was
unwilling to ask questions, which I find exceedingly frustrating.
I wonder whether any justification for classical lectures still exists.

Against this lack of learning and interaction in traditional lectures,
JiTT should lead to more effective use of our (students’ and mine)
time in classroom meetings.

Based on JiTT, I repeated the above poll in 2017 and received the
following answers:

|No. of participants | % of participants | Response option         |
|----|--------|----------------------------------------------------|
| 36 | 66.67% | I can explain Java MX via synchronized.            |
|  7 | 12.92% | I’m confused, and I’m willing to ask questions.    |
| 11 | 20.37% | I’m confused, but I’m not willing to ask questions.|

The situation seems to have improved dramatically.
The [project’s report (in German)](https://www.stifterverband.org/lehrfellows/2016/lechtenboerger)
contains more information.

# Open Educational Resources (OER)

Lots of textbooks come with Powerpoint slides to be used by
instructors, which incurs two defects.  First, Powerpoint is non-free
software, which is unacceptable for and unavailable to some of us,
while I prefer
[free software](https://fsfe.org/about/basics/freesoftware.en.html).
Second, redistribution of extended or corrected slides is not
encouraged (if permitted at all).

As I want students to work more on their own and as I want to build
upon existing sources, I was looking for introductions to operating
systems that are distributed under
[Creative Commons licenses](https://en.wikipedia.org/wiki/Creative_Commons_license).
Eventually, I found
[“Operating Systems and Middleware: Supporting Controlled Interaction” by Max Hailperin](https://gustavus.edu/mcs/max/os-book/),
 - whose contents share a large overlap with my course,
 - which is licensed under a
   [Creative Commons Attribution-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/),
 - and for which source code is available on
   [GitHub](https://github.com/Max-Hailperin/Operating-Systems-and-Middleware--Supporting-Controlled-Interaction).

I distribute my presentations (slides with embedded audio) based on
Hailperin’s book as open
educational resources (OER) under a Creative Commons license managed
with Git in this GitLab repository.
Presentations are generated automatically upon
commit and are available as [GitLab pages](https://oer.gitlab.io/OS).
Technically, presentations are generated by a so-called GitLab
Continuous Integration (CI) runner (see its
[configuration file](.gitlab-ci.yml) for details),
which in turn is based on a
[Docker](https://www.docker.com/) image that you can find in the
[repository for emacs-reveal](https://gitlab.com/oer/emacs-reveal/container_registry).  That Docker
image includes [LaTeX](https://www.latex-project.org/),
[GNU Emacs](https://www.gnu.org/software/emacs/), and necessary Emacs
packages.

To ease collaboration on my presentations, they are written in
text files using the lightweight markup language [Org Mode](http://orgmode.org/).
HTML presentations are generated  with
[emacs-reveal](https://gitlab.com/oer/emacs-reveal), which is
a [free/libre and open source software (FOSS, FLOSS)](https://en.wikipedia.org/wiki/Free_and_open-source_software)
bundle (see [this paper](https://doi.org/10.21105/jose.00050)).

This project supports collaboration along the
[5 Rs of OER](https://opencontent.org/blog/archives/3221).
If you want to retain, reuse, revise, remix, or redistribute any of my
educational resources, please check out the
[separate document CONTRIBUTING.org](CONTRIBUTING.org).

# Build OER Presentations
Install GNU Emacs and [emacs-reveal](https://gitlab.com/oer/emacs-reveal),
then clone this repository, and build presentations (file
[elisp/publish.el](elisp/publish.el) expects to find emacs-reveal in
one of two hard-coded locations: `~/.emacs.d/elpa/emacs-reveal` or
as sibling to this project (with same parent directory)):

```
<Install emacs-reveal according to its README/Howto>
<Enter directory where to clone this repository>
git clone --recursive https://gitlab.com/oer/OS.git
cd OS
emacs --batch --load elisp/publish.el
```

Open published presentations (in subdirectory `public`) in your
browser:

```
firefox public/index.html
```

Emacs-reveal has a
[howto](https://oer.gitlab.io/emacs-reveal-howto/howto.html),
which is a reveal.js presentation, generated with emacs-reveal.
