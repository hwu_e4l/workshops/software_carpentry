# -*- eval: (visual-line-mode) -*-
#+PLOT: file:"evaluation.png" set:"terminal pngcairo transparent enhanced fontscale 1.0 size 1200,900" title:"Statements by students on presentation format" ind:1 deps:(2 3 4 5 6) type:2d with:histograms set:"yrange [0:]" set:"linetype 1 lc rgb 'red'" set:"style fill solid 0.5" set:"style histogram clustered gap 1" set:"xtics nomirror rotate by 60 right out" set:"ytics nomirror out" set:"ylabel 'Percentage of responses'"
| Choice                                                     | 2017 (%) | 2018 (%) | 2019 (%) | 2020 (%) | 2021 (%) |
|------------------------------------------------------------+----------+----------+----------+----------+----------|
| I prefer HTML presentations over video                     |       43 |       48 |       70 |       52 |       24 |
| I prefer videos over HTML presentations                    |       37 |       37 |       26 |       36 |       65 |
| Different types of hyperlinks are helpful                  |          |       69 |       67 |       56 |       47 |
| Hyperlinks within same presentation are helpful            |       70 |       68 |       67 |       44 |       50 |
| Hyperlinks between presentations are helpful               |       58 |       73 |       74 |       56 |       59 |
| Hyperlinks to later slides/presentations should be removed |       20 |       29 |       22 |       44 |       24 |
| Hyperlinks to external contents should open in new tabs    |          |          |          |          |       86 |
| Audio should start automatically again                     |          |          |       22 |       20 |        0 |
| The change to linear navigation should be reverted         |          |          |          |          |       50 |
|------------------------------------------------------------+----------+----------+----------+----------+----------|
