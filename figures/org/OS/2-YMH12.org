#+PLOT: file:"2-YMH12.gp.png" script:"2-YMH12.gp"
# Data from Yang et al: When Poll is Better than Interrupt, FAST 2012, https://www.usenix.org/conference/fast12/when-poll-better-interrupt
| Operation     | Operating System | Hardware Device |
|---------------+------------------+-----------------|
| "4 KiB Async" |             4.91 |            4.10 |
| "512 B Async" |             5.01 |            2.63 |
| "4 KiB Sync"  |             1.47 |            2.91 |
| "512 B Sync"  |             1.42 |            1.48 |
