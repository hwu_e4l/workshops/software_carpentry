{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "# Functions\n",
    "\n",
    "Functions in code work like mathematical functions\n",
    "\n",
    "$$y = f(x)$$\n",
    "$$y = f(x1, x2)$$\n",
    "\n",
    "* $f()$ is a function\n",
    "* $x$ is an input (or input*s*)\n",
    "* $y$ is the returned value, or output(s)\n",
    "\n",
    "Not all functions in code take an input, or produce a usable output, but the principle is generally the same."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Example: Converting between temperature units\n",
    "\n",
    "Sometimes, you get data in the wrong unit. It's still useable data, it just needs to be converted to something useful.\n",
    "We'll start by defining a function that we'll call `fahr_to_kelvin()`, to convert temperatures from Fahrenheit to Kelvin.\n",
    "Something like this would be great for weather stations that use American equipment.\n",
    "\n",
    "The mathematical function for this would be something like $y = f(x)$ where\n",
    "\n",
    "$$f(x) = ((x - 32) \\times \\frac{5}{9}) + 273.15$$\n",
    "\n",
    "We will write this as a `Python` function, below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "def fahr_to_kelvin(temp):\n",
    "    return ((temp - 32) * (5/9)) + 273.15"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions are excellent devices for code reuse, but it is wise to test functions for correctness of output when they are written (or modified)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "freezing point of water: 273.15\n",
      "boiling point of water: 373.15\n"
     ]
    }
   ],
   "source": [
    "print('freezing point of water:', fahr_to_kelvin(32))\n",
    "print('boiling point of water:', fahr_to_kelvin(212))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Composing functions\n",
    "\n",
    "`Python` functions can be composed, just like mathematical functions:\n",
    "\n",
    "$$y = f(g(x))$$\n",
    "\n",
    "Suppose we have a function that converts Kelvin to Celsius, called `kelvin_to_celsius()`, then we could convert a temperature in fahrenheit (`temp_f`) to a temperature in celsius (`temp_c`) by executing the code:\n",
    "\n",
    "```python\n",
    "temp_c = kelvin_to_celsius(fahr_to_kelvin(temp_f))\n",
    "```\n",
    "\n",
    "We'll create this function below, and then do the calculation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "absolute zero in Celsius: -273.15\n"
     ]
    }
   ],
   "source": [
    "def kelvin_to_celsius(temp_k):\n",
    "    return temp_k - 273.15\n",
    "print('absolute zero in Celsius:', kelvin_to_celsius(0.0))    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "100.0\n"
     ]
    }
   ],
   "source": [
    "temp_f = 212.0\n",
    "temp_c = kelvin_to_celsius(fahr_to_kelvin(temp_f))\n",
    "print(temp_c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "And we can then wrap this inside a third function, `fahr_to_celsius()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "freezing point of water in Celsius: 0.0\n"
     ]
    }
   ],
   "source": [
    "def fahr_to_celsius(temp_f):\n",
    "    return kelvin_to_celsius(fahr_to_kelvin(temp_f))\n",
    "print('freezing point of water in Celsius:', fahr_to_celsius(32.0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Testing Functions\n",
    "\n",
    "Functions are excellent devices for code reuse, but once functions are written they tend to be used without checking the code. So, it is wise to test functions for correctness of output when they are written (or modified).\n",
    "\n",
    "To demonstrate this, we will write a function that centres the values in a numerical array, called `centre()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "def centre(data, desired):\n",
    "    return (data - numpy.mean(data)) + desired"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We could test this on a real dataset, *but we don't know what the answer should be*.\n",
    "\n",
    "We can use `numpy` to create an artificial test set (a matrix of zeroes) and recentre this to `3.0`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 3.  3.]\n",
      " [ 3.  3.]]\n"
     ]
    }
   ],
   "source": [
    "z = np.zeros((2, 2))\n",
    "print(centre(z, 3.0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "This looks good. So we should try it on real data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[-6.14875 -6.14875 -5.14875 ..., -3.14875 -6.14875 -6.14875]\n",
      " [-6.14875 -5.14875 -4.14875 ..., -5.14875 -6.14875 -5.14875]\n",
      " [-6.14875 -5.14875 -5.14875 ..., -4.14875 -5.14875 -5.14875]\n",
      " ..., \n",
      " [-6.14875 -5.14875 -5.14875 ..., -5.14875 -5.14875 -5.14875]\n",
      " [-6.14875 -6.14875 -6.14875 ..., -6.14875 -4.14875 -6.14875]\n",
      " [-6.14875 -6.14875 -5.14875 ..., -5.14875 -5.14875 -6.14875]]\n"
     ]
    }
   ],
   "source": [
    "data = numpy.loadtxt(fname='data/inflammation-01.csv', delimiter=',')\n",
    "print(centre(data, 0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "That looks reasonable, but as we don't have an accurate answer, how can we tell the function worked?\n",
    "\n",
    "One way is to look at bulk properties of the dataset. We'd expect the mean of the new dataset to be approximately `0.0`, and the variance of the dataset to be unchanged. Also, the range (`max - min`) should be unchanged."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "original min, mean, and max are: 0.0 6.14875 20.0\n",
      "min, mean, and max of centered data are: -6.14875 2.84217094304e-16 13.85125\n",
      "difference in standard deviations before and after: 0.0\n"
     ]
    }
   ],
   "source": [
    "centred = centre(data, 0)\n",
    "print('original min, mean, and max are:', np.min(data), np.mean(data), np.max(data))\n",
    "print('min, mean, and max of centered data are:', np.min(centred),\n",
    "      np.mean(centred), np.max(centred))\n",
    "print('difference in standard deviations before and after:', np.std(data) - np.std(centred))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "The range and variance are as expected, and the mean is acceptably close to zero.\n",
    "\n",
    "The function is probably OK, as-is."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "## Documenting Functions\n",
    "\n",
    "We can document what our function does by writing comments in the code, and this is a good thing.\n",
    "\n",
    "But `Python` allows us to document what a function does directly in the function using a *docstring*. This is a string that is put in a specific place in the function definition, and it has special properties that are useful.\n",
    "\n",
    "To add a docstring to our `centre()` function, we add a string immediately after the function declaration:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "def centre(data, desired):\n",
    "    \"\"\"Returns the array in data, recentered around the desired value.\"\"\"\n",
    "    return (data - numpy.mean(data)) + desired"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "This serves two purposes. It documents the function directly in the source code, but it also hooks that documentation into `Python`'s help system.\n",
    "\n",
    "We can ask for `help` on any function using the `help()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on function centre in module __main__:\n",
      "\n",
      "centre(data, desired)\n",
      "    Returns the array in data, recentered around the desired value.\n",
      "\n"
     ]
    }
   ],
   "source": [
    "help(centre)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Using the triple quotes (`\"\"\"`) allows us to use a multi-line string to describe the function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "def centre(data, desired):\n",
    "    \"\"\"Returns the array in data, recentered around the desired value.\n",
    "    \n",
    "    Example: centre([1, 2, 3], 0) => [-1, 0, 1]\n",
    "    \"\"\"\n",
    "    return (data - np.mean(data)) + desired"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on function centre in module __main__:\n",
      "\n",
      "centre(data, desired)\n",
      "    Returns the array in data, recentered around the desired value.\n",
      "    \n",
      "    Example: centre([1, 2, 3], 0) => [-1, 0, 1]\n",
      "\n"
     ]
    }
   ],
   "source": [
    "help(centre)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([-1.,  0.,  1.])"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "centre([1, 2, 3], 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Default Arguments\n",
    "\n",
    "So far we have named the two arguments in our `centre()` function, and we need to specify both of them when we call the function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([-1.,  0.,  1.])"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "centre([1, 2, 3], 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We can set a *default* value for function arguments when we define the function, by assigning a value in the function declaration, as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "def centre(data, desired=0.0):\n",
    "    \"\"\"Returns the array in data, recentered around the desired value.\n",
    "    \n",
    "    Example: centre([1, 2, 3], 0) => [-1, 0, 1]\n",
    "    \"\"\"\n",
    "    return (data - np.mean(data)) + desired"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Now, by default, the function will recentre the passed data to zero, without us having to specify that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([-1.,  0.,  1.])"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "centre([1, 2, 3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "The value of `desired` is automatically set to `0.0`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Readable Code\n",
    "\n",
    "The two functions that follow are computationally equivalent, but not as readable as each other."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "def s(p):\n",
    "    a = 0\n",
    "    for v in p:\n",
    "        a += v\n",
    "    m = a / len(p)\n",
    "    d = 0\n",
    "    for v in p:\n",
    "        d += (v - m) * (v - m)\n",
    "    return numpy.sqrt(d / (len(p) - 1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "def std_dev(sample):\n",
    "    \"\"\"Returns the sample standard deviation of the input.\"\"\"\n",
    "    sample_sum = 0\n",
    "    for value in sample:\n",
    "        sample_sum += value\n",
    "\n",
    "    sample_mean = sample_sum / len(sample)\n",
    "\n",
    "    sum_squared_devs = 0\n",
    "    for value in sample:\n",
    "        sum_squared_devs += (value - sample_mean) * (value - sample_mean)\n",
    "\n",
    "    return numpy.sqrt(sum_squared_devs / (len(sample) - 1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "You probably found `std_dev()` much easier to read and understand than `s()`.\n",
    "\n",
    "* both documentation and a programmer’s coding style combine to determine how easy it is for others to read and understand the programmer’s code.\n",
    "* choosing meaningful variable names and using blank spaces to break the code into logical “chunks” are helpful techniques for producing readable code.\n",
    "\n",
    "This is useful not only for sharing code with others, but also for the original programmer. If you need to revisit code that you wrote months ago and haven’t thought about since then, you will appreciate the value of readable code!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "## Function Scope\n",
    "\n",
    "Function parameters, and variables defined in functions, are not *visible* outside that function. The extent of 'visibility' of variables is called *scope*, and we say that these variables have *function scope*.\n",
    "\n",
    "For example, in the code below it may look like the contents of variables are swapped, but this only takes place *inside the function*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "In the function a: 7 b: 3\n",
      "Outside the function a: 3 b: 7\n"
     ]
    }
   ],
   "source": [
    "a = 3\n",
    "b = 7\n",
    "\n",
    "def swap(a, b):\n",
    "    temp = a\n",
    "    a = b\n",
    "    b = temp\n",
    "    print(\"In the function a:\", a, \"b:\", b)\n",
    "\n",
    "swap(a, b)\n",
    "\n",
    "print(\"Outside the function a:\", a, \"b:\", b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you need to get a variable from a function, you should `return` it from the function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "In the function a: 7 b: 3\n",
      "Outside the function a: 7 b: 3\n"
     ]
    }
   ],
   "source": [
    "a = 3\n",
    "b = 7\n",
    "\n",
    "def swap(a, b):\n",
    "    temp = a\n",
    "    a = b\n",
    "    b = temp\n",
    "    print(\"In the function a:\", a, \"b:\", b)\n",
    "    return (a, b)\n",
    "\n",
    "(a, b) = swap(a, b)\n",
    "\n",
    "print(\"Outside the function a:\", a, \"b:\", b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
