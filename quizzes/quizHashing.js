quizHashing = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Hashing is a basic technique.",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about hash functions.",
            "a": [
                {"option": "Hash functions are functions in the mathematical sense.", "correct": true},
                {"option": "Hash functions map arbitrary-sized data to fixed-sized data.", "correct": true},
                {"option": "A hash collision occurs if two pieces of data are mapped to the same hash value.", "correct": true},
                {"option": "If a hash function maps arbitrary-sized data to fixed-sized data, an infinite amount of hash collisions is guaranteed.", "correct": true},
                {"option": "As hash collisions occur, hash functions cannot be invertible.", "correct": true},
                {"option": "Given a hash value h(x), it is impossible to compute the original data x from which the hash value was computed. (This points to a fundamental difference of hashing and encryption: Given an encrypted piece of data, say e(x), it is possible to compute the original data x [using a suitable decryption function and decryption key].)", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: Most or all statements are correct.)</span> Please revisit hashing, e.g., on <a href=\"https://en.wikipedia.org/wiki/Hash_function\">Wikipedia</a>.</p>" // no comma here
        }
    ]
};
