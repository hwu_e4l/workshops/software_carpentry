# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2020 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "../config.org"
#+DATE: May 2020

#+MACRO: battlemode online
#+MACRO: etherpad [[https://pad.uni-muenster.de/]]
#+MACRO: template [[https://pad.uni-muenster.de/35cRW3lYQWqIA_ZVNCnoNw?edit][this template]]
#+MACRO: emptyex [[https://pad.uni-muenster.de/VLk5-ENqRqiD54P4tpNRfQ?edit][sample file]]
#+MACRO: datastructure file
#+MACRO: cdatastructure File
#+MACRO: datastructures files
#+MACRO: cdatastructures Files
#+MACRO: preparation1 Each player creates an empty {{{datastructure}}} to communicate shots into opponent’s territory.  Please create the file at {{{etherpad}}}.  One member of the thread team is their /communicator/.  Allow the communicator to edit the file.  With CodiMD’s default mode “Limited”, you just need to inform the communicator of the file’s URL (e.g., via e-mail or chat; however, your own team members must *not* learn this).  Enter your name in the file’s first line; that way, the communicator can more easily keep track of file ownership among your team members ({{{emptyex}}} with three actions by process and responses from communicator).
#+MACRO: preparation2 Each player creates a {{{datastructure}}} with two grids.  As this is a private data structure, you can use any format (even pen and paper).  Maybe copy the contents of {{{template}}} to a new file at {{{etherpad}}}.  Other players must not learn the contents of this data structure.
#+MACRO: preparation3 As team, place the 5 ships as described above.
#+MACRO: preparation2b The team creates a {{{datastructure}}} with two grids.  As this file is shared in your team, copy the contents of {{{template}}} to a new file at {{{etherpad}}}.  Communicate the file’s URL among members of your own team.  Opponents must not learn this URL.
#+MACRO: codimd We use collaboratively editable CodiMD documents at {{{etherpad}}}.  Use your university account to “Sign In”, then create a “New note”.  The syntax of files is MarkDown, for which the questionmark icon offers help.
#+MACRO: extralearning - Organize with fellow students in an online space

#+INCLUDE: "core/instructions-battlethreads.org"
